# How to work with `ucl-quickstart` Keycloak

## Change Keycloak admin password


**Note:** Instead of changing the password here you can also change it in the env-file.

1. Select `master` realm
2. Click on the user `admin`

|![Keycloak 1](img/keycloak_1.png)|
|---|

1. Select the tab `Credentials` and `Reset password`

|![Keycloak 2](img/keycloak_2.png)|
|---|

## How to add new users?

1. Switch to realm `zeus`
2. Select `Users`
2. Click on the `Add user` button

|![Keycloak 7](img/keycloak_7.png)|
|---|

1. Create a user with a username of your choice and make sure that the `Email verified` option is activated.
2. The user must join a group. For example, select the previously created group `/ucl-admin`

|![Keycloak 8](img/keycloak_8.png)|
|---|

1. Click on the user and select the `Credentials` tab
2. Set a new initial password with `Set password`

|![Keycloak 9](img/keycloak_9.png)|
|---|

If you activate `Temporary`, the password must be reset after the first login

|![Keycloak 10](img/keycloak_10.png)|
|---|
