# UCL Quickstart

Set up UCL locally with docker-compose. **Do not use this in production.**  
This starts frontend, backend and database of UCL with Keycloak.  
This part of UCL won't be able to run DetectionRule tests! There is no TestEngine included.  
Since there is no https used to communicate with the backend, SECURE_SSL_REDIRECT is disabled. Also, http is enabled for Keycloak and running in development mode.  
This quickstart was tested with UCL backend version 0.26.0 and UCL frontend version 0.26.0. You can find a list of all available versions here:
- [FDA frontend](https://gitlab.com/z-e-u-s/fda/fda-frontend/-/releases)
- [FDA backend](https://gitlab.com/z-e-u-s/fda/fda-backend/-/releases)
- [UCL frontend](https://gitlab.com/z-e-u-s/ucl/ucl-frontend/-/releases)
- [FDA frontend](https://gitlab.com/z-e-u-s/ucl/ucl-backend/-/releases)

## Dependencies

You must be able to reach GitLab to pull images.  
Images will be pulled by docker-compose.

## Create configuration files

Create the \*.env files for frontend, backend and keycloak.  
There are three examples with description of the keys in the root directory. (`example.env`, `keycloak.example.env` and `ucl-api.example.env`)  
There are three examples for localhost usage in the root directory. (`example.local.env`, `keycloak.example.local.env` and `ucl-api.example.local.env`)  
The Config file for docker-compose (= ucl-frontend build) must be named: `.env`  
The Keycloak Config file must be named: `keycloak.env`  
The UCL Config file must be named: `ucl-api.env`

## Configure UCL Frontend

The UCL frontend can be configured via files. There are already configuration files present in this repository for ucl-quickstart / local usage. They can be found under `/config` and are already mounted into the `ucl-frontend` container (see `docker-compose.yaml`). You can change the config in the files or mount other files in the container. Have a look at the UCL-frontend readme how to do this (https://gitlab.com/z-e-u-s/ucl/ucl-frontend).

## Persist data / Docker volumes

There are currently named volumes for the databases (UCL backend and keycloak). Adjust them according to your needs.

## Start everything

Run the docker-compose. After:

- creating .env files
- add config volume mount for UCL-frontend if needed

```console
docker-compose -f docker-compose.yaml up -d
```

## Keycloak

You can login with the admin credentials in the browser `http://localhost:8080`.

Detailed instructions on how to use the `ucl-quickstarter` keycloak can be found here: [doc/keycloak.md](doc/keycloak.md). If you know what to do, simply follow the steps below.

### Creating a user (quickstart) 

To create a user for UCL:

1. Login in keycloak with admin user.
2. Switch realm to `zeus`
3. Go to `Users`
4. Add a user.
   - Set `Username`
   - `Email`
   - `First name`
   - `Last name`.
   - Check `Email verified`.
   - Join either `ucl-standard-users`-group or add after creation the `admin`-role.

### RBAC (Role Based Access Control)

UCL uses roles to grant access to endpoints. Therefore users need the roles depending on their domain.  
There are four roles:

- ucl-use-case-requester
- ucl-security-analyst
- ucl-admin
- ucl-reader

`ucl-use-case-requester` and `ucl-security-analyst` are added to the group `Standard-Users-UCL`. If you join a user in this group he gets these roles. This is a bit easier than adding both roles to the user. The same is true for the `ucl-admin` role with the `Admin-Users-UCL` Group and for the `ucl-reader` role with the `Reader-Users-UCL` Group.

Normal users will only need `ucl-use-case-requester` and `ucl-security-analyst` roles. `ucl-reader` is a read-only role.

### Technical User / Service Account

There is a client called `ucl-automation` which is currently disabled by not allowing to log in. If you want some automation done without using a human user account you can do it with this technical user / service account.  
How to activate it:

1. Login in keycloak
2. Change realm to `zeus`
3. Go to `Clients`
4. Select `ucl-automation`
5. You are in tab `Settings`. Scroll down to `Capability config` and turn on `Client authentication`. Afterwards check `Service accounts roles` by `Authentication flow`.
6. Change the tab to `Crendentials`. Here you can copy the `Client Secret` for Login.
7. Assign wanted roles for the technical user in the tab `Service account roles`.

Keep in mind that you need the client_credential flow for technical user login.


### Additional remarks

UCL quickstart uses a HTTP Strict-Transport-Security (HSTS) policy, which is defined in the nginx configuration. This applies to your entire domain, not just the URL of the response that you set the header on. Therefore, you should only use it if your entire domain is served via HTTPS only. Browsers properly respecting the HSTS header will refuse to allow users to bypass warnings and connect to a site with an expired, self-signed, or otherwise invalid SSL certificate. If you use HSTS, make sure your certificates are in good shape and stay that way.